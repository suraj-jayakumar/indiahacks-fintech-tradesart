__author__ = 'Suraj Jayakumar'
import sqlite3
from flask import Flask, render_template
from flask import request
import requests
from requests import get
from json import loads
import pickle
# GLOBAL VARIABLES
username = ""
accno = ""
balance = 0
data = {}
data2 = []
stockSentimentValues = {}
stockVolatility = {}
stockFutureValues = {}
stockCurrentValues = {}
url = 'http://finance.google.com/finance/info?client=ig&q=NSE:'


def get_stock_details_by_symbol(symbol):
    r = get(url + symbol)
    response = r.text
    # Clean response
    response = response.replace("// [", "")
    response = response.replace("]", "")
    # Convert to JSON object from string
    jsonObj = loads(response)
    strValue = jsonObj['l']
    strValue = strValue.replace(',','')
    return float(strValue)



# initilize flask
app = Flask(__name__)


# setup the route
@app.route('/hello')
def hello():
    return "Hello, World!"

#
# @app.route('/dashboard', methods=['GET', 'POST'])
# def homeHandlerFunction(username, accno, balance):
#     return render_template('dashboard.html', username=username, accno=accno, balance=balance,data=data)
#
# @app.route('/viewstocks', methods=['GET', 'POST'])
# def viewStocks():
#     global data
#
#     if request.method == 'POST':
#         with sqlite3.connect('fintech.db') as connection:
#             crsr = connection.cursor()
#             queryop = crsr.execute("SELECT * FROM stocks WHERE name = '%s'" % username)
#             for eachrow in queryop:
#                 data[str(eachrow[1])] = float(eachrow[2])
#             connection.commit()
#             print data
#         return render_template('viewstocks.html',data=data,username=username)
#     return render_template('viewstocks.html')
#
def setOtherGlobalVariable():
    global stockVolatility,stockFutureValues,stockSentimentValues
    stockVolatility = pickle.load(open('E:\Projects\IndiaHack-Fintech Hackathon\WindowsServer\src\stockVolatility.pickle','rb'))
    stockSentimentValues = pickle.load(open('E:\Projects\IndiaHack-Fintech Hackathon\WindowsServer\src\stockSentimentValues.pickle','rb'))
    stockFutureValues = pickle.load(open('E:\Projects\IndiaHack-Fintech Hackathon\WindowsServer\src\stockFutureValues.pickle','rb'))
    return

def getRecommendations():
    global data2
    r = requests.get('http://127.0.0.1:5012/flaskServer?uname='+username)
    strOp = r.text
    strOp = strOp.replace('[','')
    strOp = strOp.replace(']','')
    strOp = strOp.replace(' ','')
    arr = strOp.split(',')
    stckNos = []
    for eachele in arr:
        stckNos.append(int(eachele))
    tempHashTable = {}
    fobj = open('E:\Projects\IndiaHack-Fintech Hackathon\WindowsServer\src\StockSymbols.txt','rb')
    i = 1
    for eachline in fobj:
        tempHashTable[i] = str(eachline).strip()
        i = i + 1
    tempstockNames = []
    for eachele in stckNos:
        tempstockNames.append(tempHashTable[eachele])

    for eachstock in tempstockNames:
        data2.append(eachstock)
        data2.append(stockSentimentValues[eachstock])
        data2.append(stockVolatility[eachstock])
        data2.append(stockCurrentValues[eachstock])
        data2.append(stockFutureValues[eachstock])
    return

def getCurrentValuesForStocks():
    fobj = open('E:\Projects\IndiaHack-Fintech Hackathon\WindowsServer\src\StockSymbols.txt','rb')
    for eachline in fobj:
        temp = str(eachline).strip()
        stockCurrentValues[temp] =  get_stock_details_by_symbol(temp)






@app.route('/', methods=['GET', 'POST'])
def home():
    global username, accno, balance,data,data2
    data = {}
    data2 = []
    username = ""
    accno = ""
    balance = 0

    if request.method == 'POST':
        username = str(request.form['username'])
        password = str(request.form['pass'])

        connection = sqlite3.connect('fintech.db')
        crsr = connection.cursor()
        print username, password
        crsr.execute("SELECT * FROM users WHERE name = '%s'" % username)
        x = crsr.fetchone()
        queryop = crsr.execute("SELECT * FROM stocks WHERE name = '%s'" % username)
        for eachrow in queryop:
            data[str(eachrow[1])] = float(eachrow[2])
        connection.commit()
        connection.close()
        accno = x[1]
        balance = x[2]
        setOtherGlobalVariable()
        getCurrentValuesForStocks()
        getRecommendations()
        return render_template('dashboard.html', username=username, accno=accno, balance=balance,data=data,data2=data2)
    return render_template('index.html')


# run the server
if __name__ == '__main__':
    app.run(debug=True)
