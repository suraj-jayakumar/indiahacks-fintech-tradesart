__author__ = 'Suraj Jayakumar'
import sqlite3
from flask import Flask,render_template
from flask import request
from requests import get
from json import loads

# CONSTANTS
url = 'http://finance.google.com/finance/info?client=ig&q=NSE:'

app = Flask(__name__)

@app.route("/api/suraj/fintech/userdetails", methods=['GET','POST'])
def getUserDetails():
    print "hi"
    connection = sqlite3.connect('fintech.db')
    uname = str(request.args.get('uname'))
    password = str(request.args.get('pass'))

    crsr = connection.cursor()
    print uname,password
    crsr.execute("SELECT * FROM users WHERE name = '%s'" % uname)
    x =  crsr.fetchone()
    connection.commit()
    connection.close()
    returnString = str(x[1])+"^"+str(x[2])
    return returnString

# @app.route("/")
# def homePage():
#     return render_template('TestUserDetailsClient.html')

@app.route("/api/suraj/fintech/stockdetails", methods=['GET', 'POST'])
def logins():
    # print "Hi"
    symbol = str(request.args.get('symbol'))
    r = get(url + symbol)
    response = r.text
    # Clean response
    response = response.replace("// [", "")
    response = response.replace("]", "")
    # Convert to JSON object from string
    jsonObj = loads(response)
    returnString = ""
    returnString = jsonObj['l'] + "^" + jsonObj['c_fix'] + "^" + jsonObj['cp_fix']
    return returnString

@app.route("/", methods=['GET','POST'])
def startPage():
    if request.method == 'POST':
        return

    return render_template('HomePage.html')




if __name__ == "__main__":
    app.run(port=5010,debug=True)
