import sqlite3

connection = sqlite3.connect('fintech.db')
crsr = connection.cursor()

q1 = "CREATE TABLE users (name text,accno text PRIMARY KEY,balance real)"

q2 = "CREATE TABLE stocks (name text ,stock text ,qty real,PRIMARY KEY(name,stock))"

crsr.execute(q1)
crsr.execute(q2)

users = [('Suraj', '12123', 56000),
         ('David', '11775', 21000),
         ('James', '10823', 17500), ]

purchases = [('Suraj', 'TATAMOTORS', 15),
             ('Suraj', 'RELIANCE', 18),
             ('Suraj', 'MARUTI', 34),
             ('Suraj', 'SUNPHARMA', 9),
             ('Suraj', 'HEROMOTOCO', 56),

             ('David', 'HINDUNILVR', 32),
             ('David', 'BHARTIARTL', 15),
             ('David', 'MARUTI', 29),
             ('David', 'AXISBANK', 17),
             ('David', 'WIPRO', 26),

             ('James', 'ITC', 5),
             ('James', 'RELIANCE', 35),
             ('James', 'HINDUNILVR', 38),
             ('James', 'TCS', 14),
             ('James', 'SBIN', 23), ]

crsr.executemany('INSERT INTO users VALUES (?,?,?)', users)

crsr.executemany('INSERT INTO stocks VALUES (?,?,?)', purchases)

connection.commit()
connection.close()
